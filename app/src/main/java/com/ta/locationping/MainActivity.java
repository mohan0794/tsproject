package com.ta.locationping;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements InternetConnectivityListener {
    Button startbutton;
    TextView tvlocationjson;
    LocationManager locationManager;
    protected JSONArray locationInfo=new JSONArray();
    int timeLapse=0;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;
    public static boolean isInternetConnected=false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startbutton = (Button) findViewById(R.id.startlocationping);
        tvlocationjson = (TextView) findViewById(R.id.tvlocationjson);
        tvlocationjson.setText("Result Will display here");

        final Context context = this;
        startbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isInternetConnected){
                    startbutton.setEnabled(false);
                    startbutton.setText("Location Fetching Started");
                    if (getLocationMode(context) == 3) {
                        Display();
                    } else {
                        dialogbox();
                    }
                }else{
                    Toast.makeText(MainActivity.this, "Please check your internet connection",Toast.LENGTH_LONG).show();
                }


            }
        });

        try {
            mInternetAvailabilityChecker = InternetAvailabilityChecker.init(getApplicationContext());
            mInternetAvailabilityChecker.addInternetConnectivityListener(this);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public int getLocationMode(Context context) {
        try {
            return Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }


    private void Display() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean st = getLocation();
        System.out.println("LOCATION = " + st);
        if (!st) {
            statusCheck();
        }
        android.location.LocationListener locationListener = new MyLocationListener();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {//600000 milli secs = 10 min
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 600000, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 600000, 0, locationListener);
        }
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            this.isInternetConnected=isConnected;
        } else {
            this.isInternetConnected=isConnected;
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast_layout,(ViewGroup) findViewById(R.id.custom_toast_layout));
            TextView tv = (TextView) layout.findViewById(R.id.txtvw);
            tv.setText("No Internet Connection");
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.BOTTOM, 0, 100);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
//            DynamicToast.make(this, "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    private class MyLocationListener implements android.location.LocationListener {

        @Override
        public void onLocationChanged(Location loc) {

            String longitude = String.valueOf(loc.getLongitude());
            String latitude = String.valueOf(loc.getLatitude());

            Log.d("ProcessFlow", "location-->" + longitude);
            Log.d("ProcessFlow", "location-->" + latitude);

            storeInlocalMemory(longitude,latitude);

//            Toast.makeText(MainActivity.this,"Latitude"+latitude+"Longitude"+longitude,Toast.LENGTH_LONG).show();

            String s = longitude + "\n" + latitude;
            Log.d("ProcessFlow", "location details>" + s);

        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d("ProcessFlow", "*********Disabled");
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d("ProcessFlow", "*********Enabled");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d("ProcessFlow", "*********Status Changed");
        }
    }


    private void dialogbox() {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("GPS");
        alert.setCancelable(false);
        alert.setMessage("Please Turn On GPS to High Accuracy Mode");
        DialogInterface.OnClickListener listener;
        alert.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                    }
                });
        alert.show();
    }


    private boolean getLocation() {

        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    public JSONArray getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(JSONArray locationInfo) {
        this.locationInfo = locationInfo;
    }


    public void storeInlocalMemory(String Longitude,String Latitude){
        timeLapse++;

        if(timeLapse==3){
            try {
                JSONObject locationObjects = new JSONObject();
                locationObjects.put("Longitude", Longitude);
                locationObjects.put("Latitude", Latitude);
                locationInfo.put(locationObjects);
                setLocationInfo(locationInfo);
                tvlocationjson.setText(getLocationInfo().toString());
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if(timeLapse>3){
            timeLapse=0;
            tvlocationjson.setText("Location Ping Started Again, Result Will display here after 30 Min");
        }else{
            try{
                JSONObject locationObjects = new JSONObject();
                locationObjects.put("Longitude",Longitude);
                locationObjects.put("Latitude",Latitude);
                locationInfo.put(locationObjects);
                setLocationInfo(locationInfo);
            }catch (Exception e){
                e.printStackTrace();
            }
        }



    }


}